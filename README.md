This is the developer version of the BOOTS package. For the public-facing version, see: https://gitlab.com/krisvillez/boots 

This toolbox provides generic tools for Bayesian Optimization in sparse data settings. It is primarily developed for the optimization of operational settings in large-scale advanced manufacturing systems but can be used in any setting where experimental optimization of operations is expensive and/or time-consuming.
