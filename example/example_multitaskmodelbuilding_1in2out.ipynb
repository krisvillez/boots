{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multi-task model building\n",
    "\n",
    "This notebook illustrates how multi-task models can be built. It also shows how a variety of multi-task Gaussian Process Regression (GPR) models can be made equivalent through specific choices of the hyperparameters. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "340404f1",
   "metadata": {},
   "source": [
    "## 0. Load packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6d5f57e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python management\n",
    "import time\n",
    "\n",
    "# Data management\n",
    "import pandas as pd\n",
    "\n",
    "# Mathematical packages:\n",
    "import numpy as np\n",
    "from scipy.stats.qmc import LatinHypercube as LHS\n",
    "import torch\n",
    "\n",
    "# Visualization\n",
    "from IPython import display\n",
    "import matplotlib\n",
    "from mpl_toolkits.mplot3d import Axes3D\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# BOOTS package imports\n",
    "from context import MultiTaskModel as MTModel\n",
    "from context import gpr_figure\n",
    "\n",
    "# Visualization setup\n",
    "matplotlib.use(\"Qt5Agg\")\n",
    "plt.ion()\n",
    "plt.close(\"all\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3ff84a2",
   "metadata": {},
   "source": [
    "## 1. Setup optimization problem\n",
    "\n",
    "### 1.1. Describe inputs and outputs\n",
    "\n",
    "The inputs and outputs are both described by means of a range ('scale minimum' and 'scale maximum'). This is used to normalize the input and output data for training purposes. In addition, a range for optimization purposes is defined for the inputs as well ('lower bound' and 'upper bound'):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34ca85e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "R = 4\n",
    "input_description = {\n",
    "    \"x\": {\"scale minimum\": 0, \"scale maximum\": 10, \"lower bound\": 1, \"upper bound\": 9}\n",
    "}\n",
    "output_description = {\n",
    "    \"q_1\": {\"scale minimum\": -10, \"scale maximum\": 10},\n",
    "    \"q_2\": {\"scale minimum\": -10, \"scale maximum\": 10},\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a09a97aa",
   "metadata": {},
   "source": [
    "### 1.2 Multi-task objective function\n",
    "\n",
    "Next we define the ground truth function that will be used to simulate the result of an experiment:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def process_sim(x):\n",
    "    f_1 = x * torch.sin(x)\n",
    "    f_2 = (12 - x) * torch.sin(12 - x)\n",
    "    return f_1, f_2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c785222e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ground truth simulation:\n",
    "N_grid = 128\n",
    "x_sim = torch.linspace(\n",
    "    input_description[\"x\"][\"scale minimum\"],\n",
    "    input_description[\"x\"][\"scale maximum\"],\n",
    "    N_grid + 1,\n",
    ")\n",
    "X_sim = torch.cat(\n",
    "    [\n",
    "        x_sim.flatten()[None, :],\n",
    "    ]\n",
    ").T\n",
    "Y_sim = process_sim(X_sim)\n",
    "\n",
    "\n",
    "df_simulation = pd.DataFrame(\n",
    "    data=torch.hstack([X_sim, Y_sim[0], Y_sim[1]]),\n",
    "    columns=dict(input_description, **output_description),\n",
    ")\n",
    "df_simulation\n",
    "\n",
    "# Make the plots\n",
    "plt.close(\"all\")\n",
    "fig = plt.figure(figsize=(10, 5))\n",
    "ax = fig.add_subplot(121)\n",
    "ax.plot(df_simulation[\"x\"].values, df_simulation[\"q_1\"].values)\n",
    "\n",
    "ax = fig.add_subplot(122)\n",
    "ax.plot(df_simulation[\"x\"].values, df_simulation[\"q_2\"].values)\n",
    "\n",
    "fig.canvas.draw()\n",
    "fig.canvas.flush_events()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0239df59",
   "metadata": {},
   "source": [
    "## 2. Data collection"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b97701a7",
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(0)\n",
    "x_min = input_description[\"x\"][\"scale minimum\"]\n",
    "x_max = input_description[\"x\"][\"scale maximum\"]\n",
    "x_ = torch.rand(10) * (x_max - x_min) + x_min\n",
    "X_ = torch.cat(\n",
    "    [\n",
    "        x_.flatten()[None, :],\n",
    "    ]\n",
    ").T\n",
    "Y_ = process_sim(X_)\n",
    "Y_ = torch.hstack([Y_[0], Y_[1]])\n",
    "df_data = pd.DataFrame(\n",
    "    data=torch.hstack([X_, Y_]),\n",
    "    columns=dict(input_description, **output_description),\n",
    ")\n",
    "\n",
    "# Make the plots\n",
    "plt.close(\"all\")\n",
    "fig, axs = plt.subplots(\n",
    "    len(output_description), len(input_description), figsize=(10, 5), squeeze=False\n",
    ")\n",
    "\n",
    "for input_idx, input_key in enumerate(input_description):\n",
    "    for output_idx, output_key in enumerate(output_description):\n",
    "        ax = axs[output_idx, input_idx]\n",
    "        ax.plot(df_simulation[\"x\"].values, df_simulation[output_key].values)\n",
    "        ax.plot(\n",
    "            df_data[input_key],\n",
    "            df_data[output_key],\n",
    "            marker=\"o\",\n",
    "            markersize=7,\n",
    "            markeredgecolor=\"magenta\",\n",
    "            markerfacecolor=\"None\",\n",
    "            markeredgewidth=2,\n",
    "            linestyle=\"None\",\n",
    "            label=\"experimental data\",\n",
    "        )\n",
    "        ax.axvline(\n",
    "            x=input_description[input_key][\"lower bound\"],  # Line on x = lower bound\n",
    "            ymin=0,  # Bottom of the plot\n",
    "            ymax=1,\n",
    "            color=\"black\",\n",
    "            linestyle=\"--\",\n",
    "        )  # Top of the plot\n",
    "        ax.axvline(\n",
    "            x=input_description[input_key][\"upper bound\"],  # Line on x = lower bound\n",
    "            ymin=0,  # Bottom of the plot\n",
    "            ymax=1,\n",
    "            color=\"black\",\n",
    "            linestyle=\"--\",\n",
    "        )  # Top of the plot\n",
    "\n",
    "    pass\n",
    "fig.canvas.draw()\n",
    "fig.canvas.flush_events()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6fd3deb4",
   "metadata": {},
   "source": [
    "## 3. Model building\n",
    "\n",
    "The selected model exhibits no correlation between the output variables or their measurement noise. Correlation only exists between measurements of the same output variable. The Matern kernel is used with equal lengthscales for the multiple inputs, resulting in a translation- and rotation-invariant model. \n",
    "\n",
    "### 3.1. Model version 1: List of independent models\n",
    "\n",
    "The lack of correlation between the output signals means that the model can be constructed as a list of independent models, containing one model for each output. The next piece of code does this and visualizes the model output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6eb75795",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Provide bounds in required format\n",
    "lb = [input_description[var][\"scale minimum\"] for var in input_description]\n",
    "ub = [input_description[var][\"scale maximum\"] for var in input_description]\n",
    "X_scale = {\"lo\": lb, \"hi\": ub}\n",
    "lb = [input_description[var][\"lower bound\"] for var in input_description]\n",
    "ub = [input_description[var][\"upper bound\"] for var in input_description]\n",
    "X_bound = {\"lo\": lb, \"hi\": ub}\n",
    "\n",
    "## Version 1: Implement model as list of multi-input single-output (MISO) models\n",
    "model_1 = MTModel(\n",
    "    X=X_,\n",
    "    Y=Y_,\n",
    "    X_bound=X_bound,\n",
    "    X_scale=X_scale,\n",
    "    model_type=\"IndependentMultiTaskList\",\n",
    ")\n",
    "\n",
    "figure = gpr_figure(\n",
    "    input_description,\n",
    "    output_description,\n",
    "    model=model_1,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3970c9f",
   "metadata": {},
   "source": [
    "Benefits of this implementation:\n",
    "* Flexibility: Truly independent model for each output, meaning that kernel structure and kernel hyper-parameters can be tuned individually for each output\n",
    "\n",
    "Drawbacks of this implementation:\n",
    "* Lack of efficiency: This is an inefficient way to store and evaluate the model if the kernel structure is the same for both outputs, as in this case.\n",
    "* No output correlation: This model cannot accomodate for correlation between the different output signals. Neither can correlation between measurement errors from different output signals be accounted for. \n",
    "* No measurement error correlation: Zero correlation between measurement errors from different output signals is assumed. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "725886ab",
   "metadata": {},
   "source": [
    "### 3.2. Model version 2: Independent outputs with shared kernel\n",
    "\n",
    "Since the kernel structure is shared between the outputs, a more memory-efficient implementation is feasible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ba56ef25",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "## Version 1: Implement model as list of multi-input single-output (MISO) models\n",
    "model_2 = MTModel(\n",
    "    X=X_,\n",
    "    Y=Y_,\n",
    "    X_bound=X_bound,\n",
    "    X_scale=X_scale,\n",
    "    model_type=\"IndependentMultiTask\",\n",
    ")\n",
    "\n",
    "figure = gpr_figure(\n",
    "    input_description,\n",
    "    output_description,\n",
    "    model=model_2,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e46516f0",
   "metadata": {},
   "source": [
    "Benefits of this implementation:\n",
    "* Efficiency: This is an efficient way to store and evaluate the model if kernel structure is the same for both outputs, as in this case.\n",
    "* Measurement error correlation: This model can be tuned to model correlation between measurement errors from different output signals for the same set of inputs. \n",
    "\n",
    "Drawbacks of this implementation:\n",
    "* Lack of flexibility: The kernel structure is shared between the output signals, thus limiting expressiveness of the model\n",
    "* No output correlation: This model cannot accomodate for correlation between the different output signals.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb039a8a",
   "metadata": {},
   "source": [
    "### 3.2. Model version 3: Correlated outputs with shared kernel\n",
    "\n",
    "The selected model can also be implemented as a model with output correlation while constraining the output covariance to be zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Version 3: Implement model as list of multi-input single-output (MISO) models\n",
    "model_3 = MTModel(\n",
    "    X=X_,\n",
    "    Y=Y_,\n",
    "    X_bound=X_bound,\n",
    "    X_scale=X_scale,\n",
    "    model_type=\"MultiTask\",\n",
    ")\n",
    "\n",
    "figure = gpr_figure(\n",
    "    input_description,\n",
    "    output_description,\n",
    "    model=model_3,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f444e699",
   "metadata": {},
   "source": [
    "Benefits of this implementation:\n",
    "* Efficiency: This is an efficient way to store and evaluate the model if kernel structure is the same for both outputs, as in this case.\n",
    "* Output correlation: This model can accomodate for correlation between the different output signals.\n",
    "* Measurement error correlation: This model can be tuned to model correlation between measurement errors from different output signals for the same set of inputs. \n",
    "\n",
    "Drawbacks of this implementation:\n",
    "* Lack of flexibility: The kernel structure is shared between the output signals, thus limiting expressiveness of the model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Illustrating model equivalence\n",
    "\n",
    "To illustrate that the three models are equivalent, the following plot shows the three model predictions (mean and 3-$\\sigma$ confidence limits) next to each other. These are visibly the same. All models describe the data equally well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "models = [model_1, model_2, model_3]\n",
    "\n",
    "plt.close(\"all\")\n",
    "[fig, axs] = plt.subplots(\n",
    "    len(output_description),\n",
    "    len(models) * len(input_description),\n",
    "    sharex=\"all\",\n",
    "    sharey=\"all\",\n",
    ")\n",
    "for model_idx, model in enumerate(models):\n",
    "    predictions = model.predict(x_sim)\n",
    "    [mn_sim, var_sim, _] = model.predict(X_sim)\n",
    "    ucb_sim = mn_sim + 3 * var_sim ** (1 / 2)\n",
    "    lcb_sim = mn_sim - 3 * var_sim ** (1 / 2)\n",
    "\n",
    "    for input_idx, input_key in enumerate(input_description):\n",
    "        for output_idx, output_key in enumerate(output_description):\n",
    "            ax = axs[output_idx, model_idx * len(input_description) + input_idx]\n",
    "            ax.plot(x_sim, mn_sim[:, output_idx], color=\"darkblue\", linestyle=\"--\")\n",
    "            ax.plot(x_sim, ucb_sim[:, output_idx], color=\"darkblue\")\n",
    "            ax.plot(x_sim, lcb_sim[:, output_idx], color=\"darkblue\")\n",
    "            ax.fill_between(\n",
    "                x_sim,\n",
    "                lcb_sim[:, output_idx],\n",
    "                ucb_sim[:, output_idx],\n",
    "                color=\"darkblue\",\n",
    "                alpha=0.1,\n",
    "            )\n",
    "            if output_idx + 1 == len(output_description):\n",
    "                ax.set_xlabel(input_key)\n",
    "                pass\n",
    "            if input_idx == 0 and model_idx == 0:\n",
    "                ax.set_ylabel(output_key)\n",
    "                pass\n",
    "            ax.plot(\n",
    "                df_data[input_key],\n",
    "                df_data[output_key],\n",
    "                marker=\"o\",\n",
    "                markersize=7,\n",
    "                markeredgecolor=\"black\",\n",
    "                markerfacecolor=\"None\",\n",
    "                markeredgewidth=2,\n",
    "                linestyle=\"None\",\n",
    "                label=\"experimental data\",\n",
    "            )\n",
    "            pass\n",
    "        pass\n",
    "\n",
    "    pass\n",
    "fig.tight_layout()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
