import os
import sys
sys.path.insert(0,
                os.path.abspath(
                    os.path.join(
                        os.path.dirname(__file__), '..')
                    ))

from boots.gpr import MultiTaskModel, MultiTaskModelPolynomial
from boots.visual import gpr_figure
