import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import torch
from matplotlib.colors import LinearSegmentedColormap

cdict_green = {
    "red": (
        (0.0, 0.0, 0.0),
        (0.5, 0.0, 0.0),
        (1.0, 0.0, 0.0),
    ),
    "green": (
        (0.0, 0.0, 0.0),
        (0.5, 0.0, 0.0),
        (1.0, 0.0, 0.0),
    ),
    "blue": (
        (0.0, 0.4, 0.4),
        (0.5, 0.4, 0.4),
        (1.0, 0.4, 0.4),
    ),
    "alpha": (
        (0.0, 1.0, 1.0),
        (0.5, 0.2, 0.2),
        (1.0, 0.1, 0.1),
    ),
}

mpl.colormaps.register(LinearSegmentedColormap("BlueAlpha", cdict_green))

cdict_green = {
    "red": (
        (0.0, 0.0, 0.0),
        (0.5, 0.0, 0.0),
        (1.0, 0.0, 0.0),
    ),
    "green": (
        (0.0, 0.4, 0.4),
        (0.5, 0.4, 0.4),
        (1.0, 0.4, 0.4),
    ),
    "blue": (
        (0.0, 0.0, 0.0),
        (0.5, 0.0, 0.0),
        (1.0, 0.0, 0.0),
    ),
    "alpha": (
        (0.0, 1.0, 1.0),
        (0.5, 0.2, 0.2),
        (1.0, 0.1, 0.1),
    ),
}

mpl.colormaps.register(LinearSegmentedColormap("GreenAlpha", cdict_green))


def distance_to_projection(X_data, x_reference, idxs=[]):
    """ """
    ONES = torch.ones(X_data.shape[0], 1)
    X_project = ONES @ x_reference
    for idx in idxs:
        X_project[:, idx] = torch.tensor(X_data[:, idx]).float()
        pass
    distance = torch.sum((X_project - X_data) ** 2, axis=1) ** (1 / 2)
    return distance


class gpr_figure:
    def __init__(
        self,
        input_description,
        output_description,
        criterion=[],
        model=[],
        x_reference=[],
    ):
        """ """
        self.input_description = input_description
        self.output_description = output_description
        self.num_input = len(input_description)
        self.num_output = len(output_description)

        if self.num_input == 1:
            if criterion == "UCB" or criterion == []:
                [fig, axs] = plt.subplots(1, self.num_output, squeeze=False)
                pass
            else:
                [fig, axs] = plt.subplots(2, self.num_output, squeeze=False)
                pass
            show_model_output = True
            pass
        else:
            numrows = self.num_input
            numcols = self.num_input * self.num_output
            [fig, axs] = plt.subplots(
                numrows, numcols, figsize=(16, 16 * numrows // numcols)
            )
            show_model_output = any([criterion == "UCB", criterion == []])
            pass

        self.fig = fig
        self.axs = axs

        self.criterion = criterion
        self.show_model_output = show_model_output

        self._add_boundlines()
        self._add_labels()

        self._update_predictions()
        self._update_ref_lines()
        self._update_design_lines()
        self._update_acquisition_function()
        self._update_data()
        self._update_new_data()

        self.fig.tight_layout()
        
        if model != []:
            self.show_model(model, x_reference=x_reference)
            pass

        pass

    def _add_labels(self):
        """ """
        axs = self.axs
        num_input = self.num_input
        num_input = self.num_input

        criterion = self.criterion

        for output_idx, output_key in enumerate(self.output_description):
            #  bivariate plots off diagonal
            keys = [key for key in self.input_description]
            for idx2, key2 in enumerate(keys):
                for idx1, key1 in enumerate(keys[:idx2]):
                    v1 = idx2
                    v2 = idx2 + idx1

                    row = num_input - v2 - 1
                    col = v1 - 1 + num_input * output_idx
                    ax = axs[row, col]
                    ax.set_xlabel(key1)
                    ax.set_ylabel(key2)

                    row = self.num_input - v1
                    col = v2 + num_input * output_idx
                    ax = axs[row, col]
                    ax.set_xlabel(key2)
                    ax.set_ylabel(key1)
                    pass
                pass

            #  univariate plots on diagonal
            for input_idx, input_key in enumerate(self.input_description):
                if self.num_input == 1:
                    two_rows = not (criterion == "UCB" or criterion == [])
                    if two_rows:
                        row = 1
                        col = output_idx
                        ax = axs[row, col]
                        ax.set_xlabel(input_key)
                        ax.set_ylabel(self.criterion)
                        lbl = criterion.replace("_", " ")
                        lbl = "log10({})".format(lbl)
                        ax.set_ylabel(lbl)
                        pass

                    row = 0
                    col = output_idx
                    pass
                else:
                    row = num_input - input_idx - 1
                    col = input_idx + num_input * output_idx
                    pass
                ax = axs[row, col]
                ax.set_xlabel(input_key)
                if any([self.num_input == 1, criterion == "UCB", criterion == []]):
                    ax.set_ylabel(output_key)
                    pass
                else:
                    lbl = criterion.replace("_", " ")
                    lbl = "log10({})".format(lbl)
                    ax.set_ylabel(lbl)
                    pass
                pass
            pass

        pass

    def show_model(self, model, x_reference=[]):
        """ """
        if x_reference == []:
            x_reference = []
            for input_idx, key in enumerate(self.input_description):
                scale_min = self.input_description[key]["scale minimum"]
                scale_max = self.input_description[key]["scale maximum"]
                x_reference.append((scale_min + scale_max) / 2)
                pass
            x_reference = torch.tensor(x_reference)[None, :]
            pass

        self._update_contours(model=model, x_reference=x_reference)
        self._update_predictions(model=model, x_reference=x_reference)
        self._update_acquisition_function(model=model, x_reference=x_reference)
        self._update_data(model=model, x_reference=x_reference)
        self._update_ref_lines(x_reference=x_reference)
        self._update_design_lines(model=[], x_design=[])
        self._update_new_data()
        pass

    def show_design(self, model, x_design):
        self._update_new_data()
        self._update_design_lines(model=model, x_design=x_design)
        pass

    def show_new_data(self, x_new, y_new):
        self._update_new_data(x_new, y_new)
        pass

    def boundlines(self, ax, lo_bound, hi_bound, lo_scale, hi_scale):
        ax.axvline(
            x=lo_bound,
            ymin=0,  # Bottom of the plot
            ymax=1,  # Top of the plot
            color="darkgreen",
            linestyle="-",
        )
        ax.axvline(
            x=hi_bound,
            ymin=0,  # Bottom of the plot
            ymax=1,  # Top of the plot
            color="darkgreen",
            linestyle="-",
        )
        ax.set_xlim([lo_scale, hi_scale])
        pass

    def _add_boundlines(self):
        """ """
        for output_idx, _ in enumerate(self.output_description):
            for input_idx, input_key in enumerate(self.input_description):
                lo_bound = self.input_description[input_key]["lower bound"]
                hi_bound = self.input_description[input_key]["upper bound"]
                lo_scale = self.input_description[input_key]["scale minimum"]
                hi_scale = self.input_description[input_key]["scale maximum"]

                if self.num_input == 1:
                    two_rows = int(
                        not (self.criterion == "UCB" or self.criterion == [])
                    )
                    col = output_idx
                    for row in range(0, two_rows + 1):
                        ax = self.axs[row, col]
                        self.boundlines(
                            ax,
                            lo_bound,
                            hi_bound,
                            lo_scale,
                            hi_scale,
                        )
                        pass
                    pass
                else:
                    row = self.num_input - input_idx - 1
                    col = input_idx + self.num_input * output_idx
                    ax = self.axs[row, col]
                    self.boundlines(
                        ax,
                        lo_bound,
                        hi_bound,
                        lo_scale,
                        hi_scale,
                    )
                    pass
                pass
            pass

        pass

    def _create_Xsim_2D(self, key1, key2, v1, v2, x_reference):
        """ """
        N = 64
        x_min = self.input_description[key1]["scale minimum"]
        x_max = self.input_description[key1]["scale maximum"]
        x1_sim = torch.linspace(x_min, x_max, N + 1)
        x_min = self.input_description[key2]["scale minimum"]
        x_max = self.input_description[key2]["scale maximum"]
        x2_sim = torch.linspace(x_min, x_max, N + 1)
        X1_sim, X2_sim = torch.meshgrid(
            x1_sim,
            x2_sim,
            indexing="ij",
        )
        X_sim = torch.ones((N + 1) * (N + 1), 1) @ x_reference
        X_sim[:, [v1, v2]] = torch.cat(
            [X1_sim.flatten()[None, :], X2_sim.flatten()[None, :]]
        ).T
        return X_sim, X1_sim, X2_sim

    def _create_Xsim_1D(self, key, v, x_reference):
        """ """
        N = 256
        X_sim = torch.ones(N + 1, 1) @ x_reference
        x_min = self.input_description[key]["scale minimum"]
        x_max = self.input_description[key]["scale maximum"]
        x_sim = torch.linspace(x_min, x_max, N + 1)[:, None]
        X_sim[:, [v]] = x_sim.float()

        return X_sim, x_sim

    def _remove_handles(self, attr_name):
        """ """
        if hasattr(self, attr_name):
            h_ = getattr(self, attr_name)
            for h in h_:
                h.remove()
                pass
            del h_
            pass
        pass

    def _update_acquisition_function(self, model=[], x_reference=[]):
        """ """
        criterion = self.criterion
        if criterion != []:
            input_keys = [key for key in self.input_description]
            output_keys = [key for key in self.output_description]
            num_input = self.num_input

            if not hasattr(self, "acqf"):
                acqf_2 = {}
                for output_idx, output_key in enumerate(output_keys):
                    #  univariate plots on diagonal
                    acqf_1 = {}
                    for input_idx, input_key in enumerate(input_keys):
                        if self.num_input == 1:
                            row = int(criterion != "UCB")
                            col = output_idx
                            pass
                        else:
                            row = num_input - input_idx - 1
                            col = input_idx + num_input * output_idx
                            pass
                        ax = self.axs[row, col]
                        label_ = self.criterion.replace("_", " ")
                        h = ax.plot(
                            0,
                            0,
                            color="blue",
                            linewidth=3,
                            linestyle="--",
                            label=label_,
                        )
                        acqf_1[input_key] = h
                        pass
                    acqf_2[output_key] = acqf_1
                    pass
                self.acqf = acqf_2
                pass

            if hasattr(self, "acqf"):
                if not (x_reference == [] or model == []):
                    for input_idx, input_key in enumerate(input_keys):
                        [X_sim, x_sim] = self._create_Xsim_1D(
                            input_key, input_idx, x_reference
                        )
                        acqf_sim = model.acquisition_function(
                            X_sim, criterion=self.criterion
                        )[0]
                        if criterion != "UCB":
                            acqf_sim = np.log10(acqf_sim + 1e-16)
                            pass
                        for output_idx, output_key in enumerate(output_keys):
                            h_ = self.acqf[output_key][input_key]
                            for h in h_:
                                h.set_xdata(x_sim)
                                h.set_ydata(acqf_sim[:, output_idx])
                                pass

                            if criterion != "UCB":
                                if num_input == 1:
                                    row = 1
                                    col = output_idx
                                    pass
                                else:
                                    row = num_input - input_idx - 1
                                    col = input_idx + num_input * output_idx
                                    pass
                                ax = self.axs[row, col]
                                y_ = acqf_sim[:, output_idx].squeeze()
                                y_max = np.max(y_)
                                y_min = np.min(y_)
                                y_limits = [np.minimum(0, y_min), y_max]
                                margin = (y_limits[1] - y_limits[0]) / 20
                                y_limits = [
                                    y_limits[0] - margin,
                                    y_limits[1] + margin,
                                ]
                                ax.set_ylim(y_limits)
                                pass
                            pass
                        pass
                    pass
                pass
            pass
        pass

    def _update_contours(self, model, x_reference):
        """ """
        input_keys = [key for key in self.input_description]
        output_keys = [key for key in self.output_description]
        criterion = self.criterion

        if len(input_keys) >= 2:
            self._remove_handles("contours")

            h_contours = []
            for output_idx, _ in enumerate(output_keys):
                for idx2, key2 in enumerate(input_keys):
                    for idx1, key1 in enumerate(input_keys[:idx2]):
                        v1 = idx2 - 1
                        v2 = idx2 + idx1
                        X_sim, X1_sim, X2_sim = self._create_Xsim_2D(
                            key1, key2, v1, v2, x_reference
                        )
                        N0 = X1_sim.shape[0]

                        mean_sim = model.predict(X_sim)[0]
                        if not criterion == []:
                            acqf_sim = model.acquisition_function(
                                X_sim, criterion=criterion
                            )[0]
                            pass
                        else:
                            acqf_sim = mean_sim
                            pass

                        if not (criterion == "UCB" or criterion == []):
                            acqf_sim = np.log10(acqf_sim + 1e-16)
                            pass

                        row = self.num_input - v2 - 1
                        col = v1 + self.num_input * output_idx
                        ax = self.axs[row, col]
                        h1 = ax.contourf(
                            X1_sim.detach().numpy(),
                            X2_sim.detach().numpy(),
                            mean_sim[:, output_idx].reshape([N0, N0]),
                            cmap="inferno",
                        )

                        row = self.num_input - v1 - 1
                        col = v2 + self.num_input * output_idx
                        ax = self.axs[row, col]
                        h2 = ax.contourf(
                            X2_sim.detach().numpy(),
                            X1_sim.detach().numpy(),
                            acqf_sim[:, output_idx].reshape([N0, N0]),
                            cmap="inferno",
                        )

                        [h_contours.append(h) for h in [h1, h2]]

                        pass
                    pass
                pass
            self.contours = h_contours
            pass

        pass

    def _update_data(self, model=[], x_reference=[]):
        """ """
        input_keys = [key for key in self.input_description]
        output_keys = [key for key in self.output_description]

        if True:
            # bivariate plots
            if not hasattr(self, "data_biplot"):
                x_ = np.linspace(0, 1, 3)
                data_biplot_3 = {}
                for output_idx, output_key in enumerate(output_keys):
                    #  bivariate plots off diagonal
                    data_biplot_2 = {}
                    for idx2, input_key2 in enumerate(input_keys):
                        data_biplot_1 = {}
                        for idx1, input_key1 in enumerate(input_keys[:idx2]):
                            data_biplot_0 = {}
                            v1 = idx2
                            v2 = idx2 + idx1

                            row = self.num_input - v2 - 1
                            col = v1 - 1 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.scatter(
                                x_,
                                x_,
                                c=x_,
                                s=32,
                                cmap="BlueAlpha",
                                zorder=11,
                                label="data",alpha=0,
                            )
                            data_biplot_0["hi"] = h

                            row = self.num_input - v1
                            col = v2 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.scatter(
                                x_,
                                x_,
                                c=x_,
                                s=32,
                                cmap="BlueAlpha",
                                zorder=11,
                                label="data",
                            )
                            data_biplot_0["lo"] = h
                            data_biplot_1[input_key1] = data_biplot_0
                            pass
                        data_biplot_2[input_key2] = data_biplot_1
                        pass
                    data_biplot_3[output_key] = data_biplot_2
                    pass

                self.data_biplot = data_biplot_3
                pass

            if not (x_reference == [] or model == []):
                X_data = model.scaler_input.inverse_transform(
                    model.train_inputs,
                )
                Y_data = model.scaler_output.inverse_transform(
                    model.train_outputs,
                )

                if hasattr(self, "data_biplot"):
                    for output_idx, output_key in enumerate(output_keys):
                        h_scatter = self.data_biplot[output_key]
                        for idx2, key2 in enumerate(input_keys):
                            for idx1, key1 in enumerate(input_keys[:idx2]):
                                D = distance_to_projection(
                                    X_data,
                                    x_reference,
                                    [idx1, idx2],
                                )
                                X1 = X_data[:, [idx1]]
                                X2 = X_data[:, [idx2]]

                                h = h_scatter[key2][key1]["hi"]
                                h.set_offsets(np.hstack([X1, X2]))
                                h.set_array(D)

                                h = h_scatter[key2][key1]["lo"]
                                h.set_offsets(np.hstack([X2, X1]))
                                h.set_array(D)

                                pass
                            pass
                        pass
                    pass
                pass
            pass

        if self.show_model_output:
            # univariate plots
            if not hasattr(self, "data_uniplot"):
                x_ = np.linspace(0, 1, 3)

                data_2 = {}
                for output_idx, output_key in enumerate(output_keys):
                    #  univariate plots on diagonal
                    data_1 = {}
                    for input_idx, input_key in enumerate(input_keys):
                        if self.num_input == 1:
                            row = 0
                            col = output_idx
                            pass
                        else:
                            row = self.num_input - input_idx - 1
                            col = input_idx + self.num_input * output_idx
                            pass
                        ax = self.axs[row, col]
                        h = ax.scatter(
                            x_,
                            x_,
                            c=x_,
                            s=32,
                            cmap="GreenAlpha",
                            zorder=11,
                            label="data",
                        )
                        data_1[input_key] = h
                        pass
                    data_2[output_key] = data_1
                    pass
                self.data_uniplot = data_2
                pass
            pass

            if not (x_reference == [] or model == []):
                X_data = model.scaler_input.inverse_transform(
                    model.train_inputs,
                )
                Y_data = model.scaler_output.inverse_transform(
                    model.train_outputs,
                )

                if hasattr(self, "data_uniplot"):
                    #  univariate plots on diagonal
                    for output_idx, output_key in enumerate(output_keys):
                        for input_idx, input_key in enumerate(input_keys):
                            D = distance_to_projection(
                                X_data,
                                x_reference,
                                [input_idx],
                            )
                            X = X_data[:, [input_idx]]
                            Y = Y_data[:, [output_idx]]

                            h = self.data_uniplot[output_key][input_key]
                            h.set_offsets(np.hstack([X, Y]))
                            #h.set_alpha(1.0)
                            h.set_array(D)
                            pass
                        pass
                    pass

                pass
            pass
        pass

    def _update_design_lines(self, model=[], x_design=[]):
        """ """
        input_keys = [key for key in self.input_description]
        output_keys = [key for key in self.output_description]

        if model == []:
            out_highlight = output_keys
            pass
        else:
            _, acqf_norm = model.acquisition_function(
                x_design, criterion=self.criterion
            )
            if self.criterion != "UCB":
                acqf_norm = torch.log10(acqf_norm + 1e-16)
                pass
            out_highlight = [output_keys[torch.argmax(acqf_norm)]]
            pass

        if not hasattr(self, "design_lines_uniplot"):
            refline_uniplot_2 = {}
            criterion = self.criterion
            for output_idx, out_key in enumerate(output_keys):
                refline_uniplot_1 = {}
                #  univariate plots off diagonal

                for input_idx, in_key in enumerate(input_keys):
                    h_ = []
                    if self.num_input == 1:
                        two_rows = not (criterion == "UCB" or criterion == [])
                        col = output_idx
                        for row in range(0, two_rows + 1):
                            ax = self.axs[row, col]
                            h = ax.axvline(
                                x=0,
                                ymin=0,
                                ymax=1,
                                color="darkgreen",
                                linestyle=":",
                                alpha=0.0,
                                zorder=99,
                            )
                            h_.append(h)
                            pass
                        pass
                    else:
                        row = self.num_input - input_idx - 1
                        col = input_idx + self.num_input * output_idx
                        ax = self.axs[row, col]
                        h = ax.axvline(
                            x=0,
                            ymin=0,
                            ymax=1,
                            color="darkgreen",
                            linestyle=":",
                            alpha=0.0,
                            zorder=99,
                        )
                        h_.append(h)
                        pass
                    refline_uniplot_1[in_key] = h_
                    pass
                refline_uniplot_2[out_key] = refline_uniplot_1

                pass
            self.design_lines_uniplot = refline_uniplot_2
            pass

        if hasattr(self, "design_lines_uniplot"):
            for output_idx, out_key in enumerate(self.output_description):
                for input_idx, in_key in enumerate(input_keys):
                    h_ = self.design_lines_uniplot[out_key][in_key]
                    alpha = x_design != [] and out_key in out_highlight
                    for h in h_:
                        h.set_alpha(alpha)
                        if alpha:
                            h.set_xdata(x_design[:, input_idx])
                            pass
                        pass
                    pass
                pass

        #  bivariate plots off diagonal
        if self.num_input >= 2:
            if not hasattr(self, "design_point_biplot"):
                design_point_biplot_3 = {}
                for output_idx, out_key in enumerate(output_keys):
                    design_point_biplot_2 = {}

                    for idx2, key2 in enumerate(input_keys):
                        design_point_biplot_1 = {}
                        for idx1, key1 in enumerate(input_keys[:idx2]):
                            design_point_biplot_0 = {}
                            v1 = idx2
                            v2 = idx2 + idx1

                            row = self.num_input - v2 - 1
                            col = v1 - 1 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.plot(
                                0,
                                0,
                                marker="o",
                                zorder=100,
                                mec="darkgreen",
                                mfc="lightgrey",
                            )
                            design_point_biplot_0["hi"] = h

                            row = self.num_input - v1
                            col = v2 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.plot(
                                0,
                                0,
                                marker="o",
                                zorder=100,
                                mec="darkgreen",
                                mfc="lightgrey",
                            )
                            design_point_biplot_0["lo"] = h

                            design_point_biplot_1[key1] = design_point_biplot_0
                            pass
                        design_point_biplot_2[key2] = design_point_biplot_1
                        pass
                    design_point_biplot_3[out_key] = design_point_biplot_2

                    pass
                self.design_point_biplot = design_point_biplot_3

            if hasattr(self, "design_point_biplot"):
                for output_idx, out_key in enumerate(output_keys):
                    for idx2, key2 in enumerate(input_keys):
                        for idx1, key1 in enumerate(input_keys[:idx2]):
                            h_ = self.design_point_biplot[out_key][key2][key1]
                            alpha = x_design != [] and out_key in out_highlight
                            for n in ["hi", "lo"]:
                                for h in h_[n]:
                                    h.update({"alpha": alpha})
                                    pass
                                pass
                            if alpha:
                                x1_ref = x_design[:, idx1]
                                x2_ref = x_design[:, idx2]
                                for h in h_["hi"]:
                                    h.set_xdata(x1_ref)
                                    h.set_ydata(x2_ref)
                                    pass
                                for h in h_["lo"]:
                                    h.set_xdata(x2_ref)
                                    h.set_ydata(x1_ref)
                                    pass
                                pass
                            pass
                        pass
                    pass
                pass

            if not hasattr(self, "design_lines_biplot"):
                refline_biplot_3 = {}
                for output_idx, out_key in enumerate(output_keys):
                    refline_biplot_2 = {}

                    for idx2, key2 in enumerate(input_keys):
                        refline_biplot_1 = {}
                        for idx1, key1 in enumerate(input_keys[:idx2]):
                            refline_biplot_0 = {}
                            v1 = idx2
                            v2 = idx2 + idx1

                            row = self.num_input - v2 - 1
                            col = v1 - 1 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.axvline(
                                x=0,
                                ymin=0,
                                ymax=1,
                                zorder=99,
                                color="darkgreen",
                                linestyle=":",
                                alpha=0.0,
                            )
                            refline_biplot_0["vline_hi"] = h
                            h = ax.axhline(
                                y=0,
                                xmin=0,
                                xmax=1,
                                zorder=99,
                                color="darkgreen",
                                linestyle=":",
                                alpha=0.0,
                            )
                            refline_biplot_0["hline_hi"] = h

                            row = self.num_input - v1
                            col = v2 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.axvline(
                                x=0,
                                ymin=0,
                                ymax=1,
                                zorder=99,
                                color="darkgreen",
                                linestyle=":",
                                alpha=0.0,
                            )
                            refline_biplot_0["vline_lo"] = h
                            h = ax.axhline(
                                y=0,
                                xmin=0,
                                xmax=1,
                                zorder=99,
                                color="darkgreen",
                                linestyle=":",
                                alpha=0.0,
                            )
                            refline_biplot_0["hline_lo"] = h

                            refline_biplot_1[key1] = refline_biplot_0
                            pass
                        refline_biplot_2[key2] = refline_biplot_1
                        pass
                    refline_biplot_3[out_key] = refline_biplot_2

                    pass
                self.design_lines_biplot = refline_biplot_3
                pass

            if hasattr(self, "design_lines_biplot"):
                for output_idx, out_key in enumerate(output_keys):
                    for idx2, key2 in enumerate(input_keys):
                        for idx1, key1 in enumerate(input_keys[:idx2]):
                            h_ = self.design_lines_biplot[out_key][key2][key1]
                            alpha = x_design != [] and out_key in out_highlight
                            for n in [
                                "vline_hi",
                                "vline_lo",
                                "hline_hi",
                                "hline_lo",
                            ]:
                                h_[n].set_alpha(alpha)
                                pass
                            if alpha:
                                x1_ref = x_design[:, idx1]
                                x2_ref = x_design[:, idx2]
                                h_["vline_hi"].set_xdata(x1_ref)
                                h_["vline_lo"].set_xdata(x2_ref)
                                h_["hline_hi"].set_ydata(x2_ref)
                                h_["hline_lo"].set_ydata(x1_ref)
                                pass
                            pass
                        pass
                    pass
                pass
            pass

        pass

    def _update_new_data(self, x_new=[], y_new=[]):
        """ """
        input_keys = [key for key in self.input_description]
        output_keys = [key for key in self.output_description]

        #  univariate plots on diagonal
        if self.show_model_output:
            if not hasattr(self, "new_data_uniplot"):
                d2_ = {}
                for output_idx, output_key in enumerate(output_keys):
                    d1_ = {}
                    for input_idx, input_key in enumerate(input_keys):
                        row = self.num_input - input_idx - 1
                        col = input_idx + self.num_input * output_idx
                        ax = self.axs[row, col]
                        h = ax.plot(
                            0,
                            0,
                            marker="o",
                            zorder=100,
                            mec="darkgreen",
                            mfc="lightgreen",
                        )
                        d1_[input_key] = h
                        pass
                    d2_[output_key] = d1_
                    pass
                self.new_data_uniplot = d2_
                pass

            if hasattr(self, "new_data_uniplot"):
                for output_idx, output_key in enumerate(output_keys):
                    for input_idx, input_key in enumerate(input_keys):
                        row = self.num_input - input_idx - 1
                        col = input_idx + self.num_input * output_idx
                        ax = self.axs[row, col]

                        h_ = self.new_data_uniplot[output_key][input_key]
                        for h in h_:
                            if x_new != [] and y_new != []:
                                x = x_new[:, [input_idx]]
                                y = y_new[:, [output_idx]]
                                h.set_xdata(x)
                                h.set_ydata(y)

                                y_lim = ax.get_ylim()
                                y_lim = (
                                    np.minimum(y_lim[0], y),
                                    np.maximum(y_lim[1], y),
                                )
                                ax.set_ylim(y_lim)

                                h.update({"alpha": 1.0})
                                pass
                            else:
                                h.update({"alpha": 0.0})
                                pass
                            pass
                        pass

                    pass
                pass

            pass

        pass

    def _update_predictions(self, model=[], x_reference=[]):
        """ """
        input_keys = [key for key in self.input_description]
        output_keys = [key for key in self.output_description]
        num_input = self.num_input

        if self.show_model_output:
            if not hasattr(self, "predictions"):
                x_ = np.linspace(0, 1, 3)
                y0_ = x_ * 0
                y1_ = y0_ - 1
                y2_ = y0_ + 1
                predictions_2 = {}
                for out_idx, out_key in enumerate(output_keys):
                    #  univariate plots on diagonal
                    predictions_1 = {}
                    for in_idx, in_key in enumerate(input_keys):
                        if num_input == 1:
                            row = 0
                            col = out_idx
                            pass
                        else:
                            row = num_input - in_idx - 1
                            col = in_idx + num_input * out_idx
                            pass

                        ax = self.axs[row, col]

                        predictions_0 = {}

                        h_ = ax.fill_between(
                            x_, y1_, y2_, color="lightgreen", alpha=0.2
                        )
                        predictions_0["fill"] = h_

                        h_ = ax.plot(
                            x_,
                            y1_,
                            x_,
                            y2_,
                            color="lightgreen",
                            linewidth=1,
                            linestyle="-",
                            label="confidence band",
                        )
                        for h in h_[1:]:
                            h.set_label("_nolegend_")
                            pass
                        predictions_0["limits"] = h_

                        h_ = ax.plot(
                            x_,
                            y0_,
                            color="lightgreen",
                            linewidth=3,
                            linestyle="-",
                            label="mean",
                        )
                        predictions_0["mean"] = h_

                        predictions_1[in_key] = predictions_0

                        pass
                    predictions_2[out_key] = predictions_1
                    pass

                self.predictions = predictions_2

                pass

            if not (x_reference == [] or model == []):
                if hasattr(self, "predictions"):
                    for in_idx, in_key in enumerate(input_keys):
                        [X_sim, x_sim] = self._create_Xsim_1D(
                            in_key, in_idx, x_reference
                        )
                        [Y_sim_mn, Y_sim_var, _] = model.predict(X_sim)
                        mn_sim = Y_sim_mn
                        ucb_sim = Y_sim_mn + 3 * Y_sim_var ** (1 / 2)
                        lcb_sim = Y_sim_mn - 3 * Y_sim_var ** (1 / 2)

                        for out_idx, out_key in enumerate(output_keys):
                            predictions = self.predictions[out_key][in_key]

                            if num_input == 1:
                                row = 0
                                col = out_idx
                                pass
                            else:
                                row = num_input - in_idx - 1
                                col = in_idx + num_input * out_idx
                                pass

                            lcb = lcb_sim[:, out_idx].squeeze()
                            ucb = ucb_sim[:, out_idx].squeeze()
                            mn = mn_sim[:, out_idx].squeeze()

                            ax = self.axs[row, col]
                            if self.show_model_output:
                                dd = [[lcb, ucb], [mn]]
                                hh = [
                                    predictions["limits"],
                                    predictions["mean"],
                                ]
                                for h_, d_ in zip(hh, dd):
                                    for h, d in zip(h_, d_):
                                        h.set_xdata(x_sim)
                                        h.set_ydata(d)
                                        pass
                                    pass
                                self._update_fill(
                                    ax,
                                    predictions["fill"],
                                    x_sim,
                                    lcb_sim[:, out_idx],
                                    ucb_sim[:, out_idx],
                                )

                                match self.criterion:
                                    case "UCB":
                                        acqf_sim = model.acquisition_function(
                                            X_sim, criterion=self.criterion
                                        )[0]
                                        acqf = acqf_sim[:, out_idx].squeeze()
                                        y_min = torch.min(lcb)
                                        y_max = np.max(acqf)
                                        y_lim = [y_min, y_max]
                                        margin = (y_lim[1] - y_lim[0]) / 20
                                        y_lim = [
                                            y_lim[0] - margin,
                                            y_lim[1] + margin,
                                        ]
                                        pass
                                    case _:
                                        y_min = torch.min(lcb)
                                        y_max = torch.max(ucb)
                                        y_lim = [y_min, y_max]
                                        margin = (y_lim[1] - y_lim[0]) / 20
                                        y_lim = (
                                            y_lim[0] - margin,
                                            y_lim[1] + margin,
                                        )
                                        pass
                                ax.set_ylim(y_lim)

                                pass
                            pass
                        pass
                    pass
                pass

            pass

        pass

    def _update_ref_lines(self, x_reference=[]):
        """ """
        input_keys = [key for key in self.input_description]
        output_keys = [key for key in self.output_description]

        #  bivariate plots off diagonal
        if self.num_input >= 2:
            if not hasattr(self, "ref_lines_biplot"):
                refline_biplot_3 = {}
                for output_idx, out_key in enumerate(output_keys):
                    refline_biplot_2 = {}

                    for idx2, key2 in enumerate(input_keys):
                        refline_biplot_1 = {}
                        for idx1, key1 in enumerate(input_keys[:idx2]):
                            refline_biplot_0 = {}
                            v1 = idx2
                            v2 = idx2 + idx1

                            row = self.num_input - v2 - 1
                            col = v1 - 1 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.axvline(
                                x=0,
                                ymin=0,
                                ymax=1,
                                color="lightgrey",
                                linestyle="-",
                            )
                            refline_biplot_0["vline_hi"] = h
                            h = ax.axhline(
                                y=0,
                                xmin=0,
                                xmax=1,
                                color="lightgrey",
                                linestyle="-",
                            )
                            refline_biplot_0["hline_hi"] = h

                            row = self.num_input - v1
                            col = v2 + self.num_input * output_idx
                            ax = self.axs[row, col]
                            h = ax.axvline(
                                x=0,
                                ymin=0,
                                ymax=1,
                                color="lightgrey",
                                linestyle="-",
                            )
                            refline_biplot_0["vline_lo"] = h
                            h = ax.axhline(
                                y=0,
                                xmin=0,
                                xmax=1,
                                color="lightgrey",
                                linestyle="-",
                            )
                            refline_biplot_0["hline_lo"] = h

                            refline_biplot_1[key1] = refline_biplot_0
                            pass
                        refline_biplot_2[key2] = refline_biplot_1
                        pass
                    refline_biplot_3[out_key] = refline_biplot_2

                    pass
                self.ref_lines_biplot = refline_biplot_3
                pass

            if not hasattr(self, "ref_lines_uniplot"):
                refline_uniplot_2 = {}
                for output_idx, out_key in enumerate(output_keys):
                    refline_uniplot_1 = {}
                    #  univariate plots off diagonal

                    for input_idx, in_key in enumerate(input_keys):
                        if self.num_input == 1:
                            row = 0
                            col = output_idx
                            pass
                        else:
                            row = self.num_input - input_idx - 1
                            col = input_idx + self.num_input * output_idx
                            pass
                        ax = self.axs[row, col]

                        h = ax.axvline(
                            x=0,
                            ymin=0,
                            ymax=1,
                            color="lightgrey",
                            linestyle="-",
                        )
                        refline_uniplot_1[in_key] = h
                        pass
                    refline_uniplot_2[out_key] = refline_uniplot_1

                    pass
                self.ref_lines_uniplot = refline_uniplot_2
                pass
            pass

        if x_reference != []:
            if hasattr(self, "ref_lines_biplot"):
                for output_idx, out_key in enumerate(output_keys):
                    for idx2, key2 in enumerate(input_keys):
                        for idx1, key1 in enumerate(input_keys[:idx2]):
                            h_ = self.ref_lines_biplot[out_key][key2][key1]
                            x1_ref = x_reference[:, idx1]
                            x2_ref = x_reference[:, idx2]
                            h_["vline_hi"].set_xdata(x1_ref)
                            h_["vline_lo"].set_xdata(x2_ref)
                            h_["hline_hi"].set_ydata(x2_ref)
                            h_["hline_lo"].set_ydata(x1_ref)
                            pass
                        pass
                    pass
                pass
            if hasattr(self, "ref_lines_uniplot"):
                for output_idx, out_key in enumerate(output_keys):
                    for input_idx, in_key in enumerate(input_keys):
                        h = self.ref_lines_uniplot[out_key][in_key]
                        h.set_xdata(x_reference[:, input_idx])
                        pass
                    pass
                pass
            pass

        pass

    def _update_fill(self, ax, h_fill, x_, lcb_, ucb_):
        """ """
        # create invisible dummy object to extract the vertices
        dummy = ax.fill_between(x_.squeeze(), lcb_, ucb_, alpha=0)
        dp = dummy.get_paths()[0]
        dummy.remove()
        # update the vertices of the PolyCollection
        h_fill.set_paths([dp.vertices])
        pass

    pass
