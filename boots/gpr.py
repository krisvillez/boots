import pyDOE2 as pyDOE
import numpy as np
import torch
import gpytorch
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PolynomialFeatures
from scipy.stats import norm
from scipy.optimize import Bounds, minimize
import warnings

from gpytorch.distributions import MultivariateNormal as MVN
from gpytorch.distributions import MultitaskMultivariateNormal as MTMVN
from gpytorch.kernels import MultitaskKernel as MTKernel

DEFAULT_LENGTHSCALE = 2 / 3
DEFAULT_OUTPUTSCALE = 1.0
DEFAULT_NOISEVAR = 0.01
NU_DEFAULT = 5 / 2


def is_psd(mat):
    symmetric = (mat == mat.T).all()
    nonnegative_eigenvalues = (torch.linalg.eigvals(mat).real >= 0).all() 
    return bool(symmetric and nonnegative_eigenvalues)


class ExactGPModel(gpytorch.models.ExactGP):
    """
    ExactGPModel is a class of single-task models available in
    gpytorch. The class defines a multi-input single-output (MISO)
    Gaussian process regression (GPR) model and use this model for prediction.
    """

    def __init__(
        self,
        train_x,
        train_y,
        kernel=None,
        mean_fun=None,
        likelihood=None,
    ):
        """
        Creates an instance of the 'ExactGPModel' class.

        Parameters
        ----------
        train_x : 2D array, input data
        train_y : 2D array, output data
        kernel : kernel function (Default =
            gpytorch.kernels.ScaleKernel(gpytorch.kernels.MaternKernel()))
        mean_fun : mean function (Default = gpytorch.means.ConstantMean())
        likelihood : likelihood function (Default =
        gpytorch.likelihoods.GaussianLikelihood())

        Returns
        -------
        model : instance of the 'ExactGPModel' class.

        """
        if likelihood is None:
            likelihood = gpytorch.likelihoods.GaussianLikelihood()
            likelihood.noise_covar.noise = torch.tensor(DEFAULT_NOISEVAR)
            pass

        super(ExactGPModel, self).__init__(
            train_x,
            train_y,
            likelihood,
        )

        # define mean:
        if mean_fun is None:
            mean_fun = gpytorch.means.ConstantMean()
            pass
        self.mean_module = mean_fun

        # define kernel:
        if kernel is None:
            base_kernel = gpytorch.kernels.MaternKernel(nu=NU_DEFAULT)
            base_kernel.lengthscale = DEFAULT_LENGTHSCALE
            kernel = gpytorch.kernels.ScaleKernel(base_kernel)
            kernel.outputscale = DEFAULT_OUTPUTSCALE
            pass
        self.covar_module = kernel

        pass

    def forward(self, x):
        """
        Compute model predictions.

        Parameters
        ----------
        x : 2D array, input data

        Returns
        -------
        prediction : multivariate normal distribution (instance of
        gpytorch.distributions.MultivariateNormal)

        """
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return MVN(mean_x, covar_x)

    pass


# We will use the simplest form of GP model, exact inference
class BatchIndependentMultitaskGPModel(gpytorch.models.ExactGP):
    """
    BatchIndependentMultitaskGPModel is a class of multi-task models available
    through gpytorch. The class defines a multi-input multi-output (MISO)
    Gaussian process regression (GPR) model and uses this model for prediction.
    The same kernel structure is used for all outputs. However, the outputs are
    considered independent of each other and are described by distinct
    hyperparameter values
    """

    def __init__(
        self,
        train_x,
        train_y,
        kernel=None,
        mean_fun=None,
        likelihood=None,
        noise_rank=0,
    ):
        """
        Creates an instance of the 'BatchIndependentMultitaskGPModel' class.

        Parameters
        ----------
        train_x : 2D array, input data
        train_y : 2D array, output data
        kernel : kernel function (Default =
            gpytorch.kernels.ScaleKernel(gpytorch.kernels.MaternKernel()))
        mean_fun : mean function (Default = gpytorch.means.ConstantMean())
        likelihood : likelihood function (Default =
        gpytorch.likelihoods.MultitaskGaussianLikelihood())
        noise_rank : rank of the noise covariance matrix (Default = 0). If set
        to 0, then a diagonal noise covariance matrix is fit.

        Returns
        -------
        model : instance of the BatchIndependentMultitaskGPModel class

        """
        num_output = train_y.shape[-1]
        if likelihood is None:
            likelihood = gpytorch.likelihoods.MultitaskGaussianLikelihood(
                num_tasks=num_output,
                has_global_noise=False,
                rank=noise_rank,
            )
            likelihood.task_noises = torch.ones(num_output) * DEFAULT_NOISEVAR
            pass

        super(BatchIndependentMultitaskGPModel, self).__init__(
            train_x,
            train_y,
            likelihood,
        )

        # define multi-task mean:
        if mean_fun is None:
            mean_fun = gpytorch.means.ConstantMean(
                batch_shape=torch.Size([num_output]),
            )
            pass
        self.mean_module = mean_fun

        # define multi-task kernel:
        if kernel is None:
            base_kernel = gpytorch.kernels.MaternKernel(
                nu=NU_DEFAULT,
                batch_shape=torch.Size(
                    [num_output],
                ),
            )
            base_kernel.lengthscale = DEFAULT_LENGTHSCALE
            sz_ = torch.Size([num_output])
            kernel = gpytorch.kernels.ScaleKernel(
                base_kernel,
                batch_shape=sz_,
            )
            kernel.outputscale = DEFAULT_OUTPUTSCALE
            pass
        self.covar_module = kernel

        pass

    def forward(self, x):
        """
        Compute model predictions.

        Parameters
        ----------
        x : 2D array, input data

        Returns
        -------
        prediction : multivariate normal distribution (instance of
        gpytorch.distributions.MultitaskMultivariateNormal)

        """
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        mvn = MVN(mean_x, covar_x)
        dist = MTMVN.from_batch_mvn(mvn)
        return dist

    pass


class MultitaskGPModel(gpytorch.models.ExactGP):
    """
    MultitaskGPModel is a class of multi-task models available
    through gpytorch. The class defines a multi-input multi-output (MIMO)
    Gaussian process regression (GPR) model and uses this model for prediction.
    A joint kernel structure is used so that correlation between outputs can be
    captured in this model.
    """

    def __init__(
        self,
        train_x,
        train_y,
        kernel=None,
        mean_fun=None,
        likelihood=None,
        noise_rank=0,
    ):
        """
        Creates an instance of the 'MultitaskGPModel' class.

        Parameters
        ----------
        train_x : 2D array, input data
        train_y : 2D array, output data
        kernel : kernel function (Default =
            gpytorch.kernels.ScaleKernel(gpytorch.kernels.MaternKernel()))
        mean_fun : mean function (Default = gpytorch.means.ConstantMean())
        likelihood : likelihood function (Default =
        gpytorch.likelihoods.MultitaskGaussianLikelihood())
        noise_rank : rank of the noise covariance matrix (Default = 0). If set
        to 0, then a diagonal noise covariance matrix is fit.

        Returns
        -------
        model : instance of the MultitaskGPModel class

        """
        num_output = train_y.shape[-1]
        if likelihood is None:
            likelihood = gpytorch.likelihoods.MultitaskGaussianLikelihood(
                num_tasks=num_output,
                has_global_noise=False,
                rank=noise_rank,
            )
            likelihood.task_noises = torch.ones(num_output) * DEFAULT_NOISEVAR
            pass

        super(MultitaskGPModel, self).__init__(train_x, train_y, likelihood)

        # define single-task mean:
        if mean_fun is None:
            mean_fun = gpytorch.means.ConstantMean()
            pass

        # define single-task kernel:
        if kernel is None:
            base_kernel = gpytorch.kernels.MaternKernel(nu=NU_DEFAULT)
            base_kernel.lengthscale = DEFAULT_LENGTHSCALE
            kernel = gpytorch.kernels.ScaleKernel(base_kernel)
            kernel.outputscale = DEFAULT_OUTPUTSCALE
            pass

        # define multi-task mean and kernel:
        self.mean_module = gpytorch.means.MultitaskMean(
            mean_fun,
            num_tasks=num_output,
        )
        multittask_kernel_function = MTKernel(
            kernel,
            num_tasks=num_output,
            rank=num_output,
        )
        # initialize without output correlation
        covar_ = torch.nn.Parameter(torch.zeros(num_output, num_output))
        var_ = torch.ones(num_output) * (DEFAULT_OUTPUTSCALE)
        multittask_kernel_function.task_covar_module.covar_factor = covar_
        multittask_kernel_function.task_covar_module.var = var_
        self.covar_module = multittask_kernel_function
        pass

    def forward(self, x):
        """
        Compute model predictions.

        Parameters
        ----------
        x : 2D array, input data

        Returns
        -------
        prediction : multivariate normal distribution (instance of
        gpytorch.distributions.MultitaskMultivariateNormal)

        """
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return MTMVN(mean_x, covar_x)

    pass


def CreateMultiTaskModel(
    train_inputs,
    train_outputs,
    model_type="IndependentMultiTask",
    kernel=None,
    mean_fun=None,
):
    """
    Create a multi-input multi-output Gaussian process regression (GPR) model
    of the chosen type. One can choose between 'IndependentMultiTask',
    'IndependentMultiTaskList', 'MultiTask' model structures.

    Parameters
    ----------
    train_inputs : 2D array, input data
    train_outputs : 2D array, output data
    model_type : string defining the model type, must be
        'IndependentMultiTask', 'IndependentMultiTaskList', or 'MultiTask'
        (Default = 'IndependentMultiTask').
    kernel : kernel function (Default =
        gpytorch.kernels.ScaleKernel(gpytorch.kernels.MaternKernel()))
    mean_fun : mean function (Default = gpytorch.means.ConstantMean())

    Returns
    -------
    model : GPR model

    """
    # num_input = train_inputs.shape[1]
    num_output = train_outputs.shape[1]
    if model_type == "IndependentMultiTaskList":
        models = []
        for index_output in range(num_output):
            train_output = train_outputs[:, index_output]
            model = ExactGPModel(
                train_inputs,
                train_output,
                kernel=kernel,
                mean_fun=mean_fun,
            )
            models.append(model)
            pass

        model = gpytorch.models.IndependentModelList(*models)
        pass
    elif model_type == "IndependentMultiTask":
        model = BatchIndependentMultitaskGPModel(
            train_inputs,
            train_outputs,
            kernel=kernel,
            mean_fun=mean_fun,
        )
        pass
    else:
        model_type = "MultiTask"
        model = MultitaskGPModel(
            train_inputs,
            train_outputs,
            kernel=kernel,
            mean_fun=mean_fun,
        )
        pass

    return model


class MultiTaskModel:
    """
    MultiTaskModel is generic class for multi-task models available in
    gpytorch. One can choose between 'IndependentMultiTask',
    'IndependentMultiTaskList', 'MultiTask' model structures. The class
    provides functionality to define and train multi-input multi-output (MIMO)
    Gaussian process regression (GPR) models and use this model for prediction
    and optimization.
    """

    def __init__(
        self,
        X,
        Y,
        X_scale,
        X_bound=None,
        model_type="IndependentMultiTask",
        kernel=None,
        mean_fun=None,
    ):
        """
        Creates an instance of the 'MultiTaskModel' class.

        Parameters
        ----------
        X : 2D array, input data
        Y : 2D array, output data
        X_scale : dictionary describing how the inputs should be normalized.
            X_scale['lo'] and X_scale['hi'] define the values scaled to -1 and
            +1, respectively.
        X_bound : dictionary describing the input range available for
            optimization. X_bound['lo'] and X_bound['hi'] define the lower and
            upper bounds in the original scale of the input data (Default =
            X_scale).
        model_type : string defining the model type, must be
            'IndependentMultiTask', 'IndependentMultiTaskList', or 'MultiTask'
            (Default = 'IndependentMultiTask').
        kernel : kernel function (Default =
            gpytorch.kernels.ScaleKernel(gpytorch.kernels.MaternKernel()))
        mean_fun : mean function (Default = gpytorch.means.ConstantMean())

        Returns
        -------
        model : instance of the MultiTaskModel class

        """
        num_input = X.shape[1]
        num_output = Y.shape[1]

        # TODO: Enable option to leave X_scale empty

        if X_bound is None:
            X_bound = X_scale
            pass

        self.X_scale = X_scale
        self.X_bound = X_bound

        # construct input scaler - based on user input X_bounds:
        if num_input <= 1:
            U_valid = np.array([0, 1]).reshape(-1, 1)
            pass
        else:
            U_valid = pyDOE.ccdesign(
                num_input,
                face="faced",
                center=(1, 0),
            )
            pass

        X_hi = np.array(X_scale["hi"])
        X_lo = np.array(X_scale["lo"])
        X_valid = (X_hi + X_lo) / 2 + U_valid * (X_hi - X_lo) / 2
        scaler_input = MinMaxScaler(feature_range=(-1, +1))
        scaler_input.fit(X_valid)
        # apply input scaler to inputs, convert to tensor
        X_train = torch.from_numpy(scaler_input.transform(X))

        # construct output scaler
        # - train on and apply to available data Y, convert to tensor:
        scaler_output = MinMaxScaler(feature_range=(-1, +1))
        Y_train = torch.from_numpy(scaler_output.fit_transform(Y))

        self.num_input = num_input
        self.num_output = num_output
        self.scaler_input = scaler_input
        self.scaler_output = scaler_output
        self.train_inputs = X_train.float()
        self.train_outputs = Y_train.float()

        model = CreateMultiTaskModel(
            self.train_inputs,
            self.train_outputs,
            model_type=model_type,
            kernel=kernel,
            mean_fun=mean_fun,
        )

        self.model_type = model_type
        self.model = model
        # self.likelihood = likelihood
        self.model.eval()

        pass

    def train(self, verbose=True):
        """
        Trains the model.

        Parameters
        ----------
        verbose : boolean, whether or not to print out intermediate
        optimization results (Default = True)

        Returns
        -------

        """
        # Step 2c - train model
        self.model.train()

        if self.model_type == "IndependentMultiTaskList":
            mll = gpytorch.mlls.SumMarginalLogLikelihood(
                self.model.likelihood,
                self.model,
            )
            pass
        else:
            mll = gpytorch.mlls.ExactMarginalLogLikelihood(
                self.model.likelihood,
                self.model,
            )
            pass

        output = self.model(*self.model.train_inputs)
        loss = -mll(output, self.model.train_targets)
        loss.item()

        lr = 0.1
        optimizer = torch.optim.SGD(
            self.model.parameters(),
            lr=lr,
            momentum=0.01,
            nesterov=True,
        )

        abs_tol = 1e-5
        training_itr_max = 1000

        itr = 0
        abs_change = torch.inf
        loss_value = torch.inf

        while itr < training_itr_max and abs_change >= abs_tol:
            itr += 1
            self.model.train()
            optimizer.zero_grad()
            output = self.model(*self.model.train_inputs)
            loss = -mll(output, self.model.train_targets)
            loss.backward()

            optimizer.step()  # get loss, use to update wts

            # Zero gradients from previous iteration
            optimizer.zero_grad()
            # Output from model
            output = self.model(*self.model.train_inputs)
            # Calc loss and backprop gradients
            loss_prev = loss
            loss = -mll(output, self.model.train_targets)

            loss_value = loss.item()
            abs_change = torch.abs(loss_prev - loss)

            if verbose and itr % 200 == 0:
                print(
                    "Iter %d/%d - Loss: %.3f"
                    % (
                        itr,
                        training_itr_max,
                        loss_value,
                    )
                )
                pass
            pass

        if verbose:
            print(
                "Converged at iteration %d/%d - Loss: %.3f"
                % (
                    itr,
                    training_itr_max,
                    loss_value,
                )
            )
            pass

        self.model.eval()
        self.task_noise_var = self.model.likelihood.task_noises

        pass

    def predict(
        self,
        inputs,
        return_covariance=False,
        original_scale=True,
    ):
        """
        Predict outputs using the model at provided values for the inputs.

        Parameters
        ----------
        inputs : 2D array of input values
        return_covariance : Boolean (Default = False)
        original_scale : Boolean (Default = True)

        Returns
        -------
        mean_y : posterior values for the mean
        variance_y : posterior values for the variance
        covariance_y : posterior covariance matrix (produced only when
            return_covariance=False)
        prediction : posterior distribution as list of instances of
            gpytorch.distributions.multitask_multivariate_normal.
            The length of the list matches the number of outputs when the
            model type is 'IndependentMultiTask', and equals 1 otherwise.

        """
        self.model.eval()

        num_input = self.num_input
        num_output = self.num_output

        X = torch.from_numpy(
            self.scaler_input.transform(inputs.reshape([-1, num_input]))
        ).float()
        num_sample = X.shape[0]

        if self.model_type == "IndependentMultiTaskList":
            X_list = [X] * num_output
            prediction = self.model(*X_list)

            mean_gp = torch.stack([p.mean for p in prediction]).T
            variance_gp = torch.stack([p.variance for p in prediction]).T

            N = num_output * num_sample
            if return_covariance:
                covariance_gp = torch.zeros(N, N)
                for index_output in range(num_output):
                    covar = prediction[index_output].covariance_matrix
                    covariance_gp[
                        index_output::num_output, index_output::num_output
                    ] = covar
                    pass
                pass
            pass
        else:
            prediction = self.model(X)
            mean_gp = prediction.mean
            variance_gp = prediction.variance
            if return_covariance:
                covariance_gp = prediction.covariance_matrix
                pass
            prediction = [prediction]
            pass

        if original_scale:
            mean_y = torch.from_numpy(
                self.scaler_output.inverse_transform(mean_gp.detach().numpy())
            ).float()
            scale_ = self.scaler_output.scale_
            variance_y = torch.from_numpy(
                variance_gp.detach().numpy() / scale_**2
            ).float()
            pass
        else:
            mean_y = mean_gp.float()
            variance_y = variance_gp.float()
            pass
        mean_y = torch.atleast_2d(mean_y)
        variance_y = torch.atleast_2d(variance_y)

        if return_covariance:
            scale_tiled = np.tile(scale_, num_sample)
            if original_scale:
                covariance_y = covariance_gp.detach().numpy() / (
                    scale_tiled[:, None] @ scale_tiled[:, None].T
                )
                pass
            else:
                covariance_y = covariance_gp
                pass

            return mean_y, variance_y, covariance_y, prediction
        else:
            return mean_y, variance_y, prediction

        pass

    def acquisition_function(
        self,
        X_test,
        criterion="UCB",
        direction="max",
        level=6,
    ):
        """
        Computes an acquisition function with the model.

        Parameters
        ----------
        X_test : 2D array of model inputs
        criterion: string defining the acquisition function. Supported values
            are 'UCB' (confidence bound), 'EI' (expected improvement), and 'PI'
            (probability of improvement). (Default = 'UCB')
        direction: determine whether the model output should be minimized
            ("min") or maximized ("max"). Ignored when criterion = 'UCB'.
            (Default='max')
        level: confidence level as multiples of the standard deviations
            (e.g. for 3-sigma confidence band, set level=3). Only used when
            criterion = 'UCB'. (Default=6)

        Returns
        -------
        criterion_value : value of the acquisition function for every row in
            X_test and every model output
        criterion_value_normalized : normalized values of the acquisition
            function. The values are scaled by the noise standard deviation
            parameter in the model corresponding to the output.

        """
        X_train = self.train_inputs

        std_noise = self.task_noise_var ** (1 / 2)

        assert criterion in [
            "EI",
            "PI",
            "UCB",
        ], "{} is not a suitable criterion".format(criterion)

        if criterion == "UCB":
            mean_y_test, variance_y_test, _ = self.predict(X_test)
            stddev_y_test = variance_y_test ** (1 / 2)
            mean_y_test = mean_y_test.detach().numpy()
            stddev_y_test = stddev_y_test.detach().numpy()

            criterion_value = mean_y_test + level * stddev_y_test

            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                mean_y, variance_y, _ = self.predict(
                    self.scaler_input.inverse_transform(X_train),
                    original_scale=False,
                )
                mean_y_test, variance_y_test, _ = self.predict(
                    X_test, original_scale=False
                )
                pass
            stddev_y_test = variance_y_test ** (1 / 2)
            criterion_scaled = mean_y_test + level * stddev_y_test
            mean_y_train_max = torch.max(mean_y, axis=0).values

            criterion_value_normalized = (
                (criterion_scaled - mean_y_train_max) / 2 / std_noise
            )

            pass
        else:
            mn_y_test, variance_y_test, _ = self.predict(
                X_test,
                original_scale=False,
            )
            stdev_y_test = variance_y_test ** (1 / 2)
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                mean_y, variance_y, _ = self.predict(
                    self.scaler_input.inverse_transform(X_train),
                    original_scale=False,
                )
                pass
            if direction == "min":
                best_y = torch.min(mean_y)
                pass
            elif direction == "max":
                best_y = torch.max(mean_y)
                pass
            else:
                print("unknown option")
                pass

            mean_z = (best_y - mn_y_test) / stdev_y_test
            mean_z_tensor = mean_z.detach().numpy()

            standard_norm_dist = norm()
            norm_cdf = torch.tensor(standard_norm_dist.cdf(-mean_z_tensor))

            if criterion == "PI":
                criterion_value = norm_cdf.detach().numpy()
                criterion_value_normalized = norm_cdf
                pass
            elif criterion == "EI":
                norm_pdf = torch.tensor(standard_norm_dist.pdf(-mean_z_tensor))
                EI = (mn_y_test - best_y) * norm_cdf + stdev_y_test * norm_pdf
                criterion_value = EI.detach().numpy()
                criterion_value_normalized = EI / std_noise
                pass
            else:
                criterion_value = []
                criterion_value_normalized = []
                pass
            pass

        return criterion_value, criterion_value_normalized

    def search(
        self,
        criterion="UCB",
        direction="max",
        level=6,
        method="Powell",
    ):
        """
        Search for input vector that optimizes the acquisition function. For
        multi-output models, the normalized acquisition function is optimized.

        Parameters
        ----------
        criterion: string defining the acquisition function. Supported values
            are 'UCB' (confidence bound), 'EI' (expected improvement), and 'PI'
            (probability of improvement). (Default = 'UCB')
        direction: determine whether the model output should be minimized
            ("min") or maximized ("max"). Ignored when criterion = 'UCB'.
            (Default='max')
        level: confidence level as multiples of the standard deviations
            (e.g. for 3-sigma confidence band, set level=3). Only used when
            criterion = 'UCB'. (Default=6)

        Returns
        -------
        res : dictionary containing the optimal model inputs ('x'), optimized
            acquisition function value ('fun'), and the output which produces
            the optimal acquisition function value ('best_output_key').

        """
        # Optimize the UCB/EI
        X_bound = self.X_bound

        x_init = []
        for lb, ub in zip(X_bound["lo"], X_bound["hi"]):
            x_init.append(np.random.rand(1) * (ub - lb) + lb)
            pass
        x_init = np.array(x_init).squeeze()

        res_ = []
        for output_idx in range(self.num_output):

            def objfun(x):
                acq = self.acquisition_function(
                    x,
                    criterion=criterion,
                    direction=direction,
                    level=level,
                )
                acqfuns = acq[1].detach().numpy()
                if criterion != "UCB":
                    acqfuns = np.log10(acqfuns + 1e-12)
                    pass
                objfun_value = -acqfuns[:, output_idx]
                return objfun_value

            bounds = Bounds(lb=X_bound["lo"], ub=X_bound["hi"])
            res = minimize(
                objfun,
                bounds=bounds,
                method=method,
                x0=x_init,
            )
            res_.append(res)
            pass
        fun_values = np.array([r.fun for r in res_]).squeeze()
        best_output_idx = np.argmin(fun_values)
        res = res_[best_output_idx]
        res_ = {
            "x": res.x,
            "fun": res.fun,
            "best_output_key": best_output_idx,
        }
        return res_

    pass


class MultiTaskModelPolynomial(MultiTaskModel):
    """
    MultiTaskModelPolynomial is a class for multi-task polynomial models using
    gpytorch. The class provides functionality to define and train multi-input
    multi-output (MIMO) polynomial regression models and use this model for
    prediction and optimization.
    """

    def __init__(
        self,
        X,
        Y,
        X_scale,
        X_bound,
        degree=2,
    ):
        """
        Creates an instance of the 'MultiTaskModel' class.

        Parameters
        ----------
        X : 2D array, input data
        Y : 2D array, output data
        X_scale : dictionary describing how the inputs should be normalized.
            X_scale['lo'] and X_scale['hi'] define the values scaled to -1 and
            +1, respectively.
        X_bound : dictionary describing the input range available for
            optimization. X_bound['lo'] and X_bound['hi'] define the lower and
            upper bounds in the original scale of the input data (Default =
            X_scale).
        degree : polynomial degree

        Returns
        -------
        instance : instance of the class

        """
        if X_bound is None:
            X_bound = X_scale
            pass

        self.X_scale = X_scale
        self.X_bound = X_bound

        num_input = X.shape[1]
        num_output = Y.shape[1]

        # construct input scaler - based on user input X_bounds:
        if num_input <= 1:
            U_valid = np.array([0, 1]).reshape(-1, 1)
            pass
        else:
            U_valid = pyDOE.ccdesign(
                num_input,
                face="faced",
                center=(1, 0),
            )
            pass

        X_hi = np.array(X_scale["hi"])
        X_lo = np.array(X_scale["lo"])
        X_valid = (X_hi + X_lo) / 2 + U_valid * (X_hi - X_lo) / 2
        scaler_input = MinMaxScaler(feature_range=(-1, +1))
        scaler_input.fit(X_valid)
        # apply input scaler to inputs, convert to tensor
        X_train = torch.from_numpy(scaler_input.transform(X))

        # construct output scaler
        # - train on and apply to available data Y, and convert to tensor:
        scaler_output = MinMaxScaler(feature_range=(-1, +1))
        Y_train = torch.from_numpy(scaler_output.fit_transform(Y))

        self.degree = degree
        self.num_input = num_input
        self.num_output = num_output
        self.scaler_input = scaler_input
        self.scaler_output = scaler_output
        self.train_inputs = X_train.float()
        self.train_outputs = Y_train.float()

        self.train()
        pass

    def train(self):
        """
        Trains the model.

        Parameters
        ----------

        Returns
        -------

        """
        poly = PolynomialFeatures(self.degree)
        F_train = torch.tensor(
            poly.fit_transform(
                self.train_inputs.detach().numpy(),
            )
        )

        # training
        sigma2_noise = 1e-4
        P = torch.linalg.pinv(F_train)
        beta_mean = P @ self.train_outputs
        y_mean = P.T @ beta_mean
        dev = y_mean - self.train_outputs
        sigma2_noise = torch.mean(dev**2, axis=0)
        cov = P @ P.T
        beta_covariance = torch.kron(cov, torch.diag(sigma2_noise))

        self.poly = poly
        self.model_type = "polynomial"
        self.beta_mean = beta_mean
        self.beta_covariance = beta_covariance
        self.task_noise_var = sigma2_noise

        pass

    def predict(
        self,
        inputs,
        return_covariance=False,
        original_scale=True,
    ):
        """
        Predict outputs using the model at provided values for the inputs.

        Parameters
        ----------
        inputs : 2D array of input values
        return_covariance : Boolean (Default = False)
        original_scale : Boolean (Default = True)

        Returns
        -------
        mean_y : posterior values for the mean
        variance_y : posterior values for the variance
        covariance_y : posterior covariance matrix (produced only when
            return_covariance=False)
        predictions : posterior distribution of the polynomial coefficients as
            a list of gpytorch.distributions.multitask_multivariate_normal. 
            Empty when the number of coefficients is larger or equal to the 
            number of samples or when the posterior covariance is not positive 
            semi-definite.

        """
        num_input = self.num_input
        num_output = self.num_output

        X = torch.from_numpy(
            self.scaler_input.transform(
                inputs.reshape([-1, num_input]),
            )
        ).float()
        num_sample = X.shape[0]

        X = X.reshape([-1, num_input])
        F = self.poly.transform(X)
        F = torch.from_numpy(F).float()
        mean_gp = F @ self.beta_mean
        FF = torch.kron(F, torch.eye(num_output))
        covariance_gp = FF @ self.beta_covariance @ FF.T
        variance_gp = torch.diag(covariance_gp).reshape([-1, num_output])

        if original_scale:
            mean_y = torch.from_numpy(
                self.scaler_output.inverse_transform(mean_gp.detach().numpy())
            ).float()
            scale_ = self.scaler_output.scale_
            variance_y = torch.from_numpy(
                variance_gp.detach().numpy() / scale_**2
            ).float()
            pass
        else:
            mean_y = mean_gp.float()
            variance_y = variance_gp.float()
            pass

        mean_y = torch.atleast_2d(mean_y)
        variance_y = torch.atleast_2d(variance_y)

        if is_psd(self.beta_covariance) and self.train_inputs.shape[0]>self.beta_mean.shape[0]:
            predictions = [MTMVN(self.beta_mean, self.beta_covariance)]
            pass
        else:
            predictions = []
            pass

        if return_covariance:
            scale_tiled = np.tile(scale_, num_sample)
            if original_scale:
                covariance_y = covariance_gp.detach().numpy() / (
                    scale_tiled[:, None] @ scale_tiled[:, None].T
                )
                pass
            else:
                covariance_y = covariance_gp
                pass
            pass
            return mean_y, variance_y, covariance_y, predictions
        else:
            return mean_y, variance_y, predictions

    pass
